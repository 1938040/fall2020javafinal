
public class Recursion {
	/**
	 * Recursively counts how many words have w in them
	 * @param String[] words, int n
	 */
	public static int recursiveCount(String[] words,int n) {
		int length=words.length;
		int result=0;
		
		if(length==1) {
			String s=words[1].substring(n);
			if(s.contains("w"))
				return 1;
			else
				return 0;
		}
		else if(length==0) {
			return 0;
		}
		else if(length>1) {
			String[] a=new String[length/2];
			String[] b=new String[length/2-1];
			for(int i=0;i<length/2;i++) {
				a[i]=words[i];
			}
			for(int i=length/2;i<length;i++) {
				b[i]=words[i];
			}
			return recursiveCount(a,n)+recursiveCount(b,n);
		}
		return result;
		
	}
}
