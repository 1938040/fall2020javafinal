package diceroll;
import java.util.Random;

public class diceroll {
	private int cash;
	private Random ran=new Random();
	
	public diceroll() {
		cash=250;
	}
	public String playRound(String choice,int bet) {
		int o=(ran.nextInt(2));
		String result="even";
		if (o==1) {
			result="odd";
		}
		
		String text="error";
		if(result==choice) {
			text="You Win!";
			win(bet);	
		}
		else {
			text="You Lose";
			loss(bet);
		}
		return text;
	}
	public int getCash() {
		return cash;
	}
	public void win(int bet) {
		cash=cash+bet;
	}
	public void loss(int bet) {
		cash=cash-bet;
	}
}
