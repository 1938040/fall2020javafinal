package diceroll;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class userchoice implements EventHandler<ActionEvent>{
	private static diceroll game;
	private static TextField msg;
	private static TextField cash;
	private Button choice;
	private static TextField bet;
	/**
	 * static fields
	 * @param diceroll d,TextField msg,TextField cash,TextField bet
	 */
	public userchoice(diceroll d,TextField msg,TextField cash,TextField bet) {
		this.msg=msg;
		this.cash=cash;
		this.bet=bet;
		userchoice.game=d;
	}
	/**
	 * adds button
	 * @param Button choice
	 */
	public userchoice(Button choice) {
		this.choice=choice;
	}
	/**
	 * handles action event
	 * @param ActionEvent e
	 */
	@Override
	public void handle(ActionEvent e) {
		
		try {
			int b=Integer.parseInt(bet.getText());
			if(game.getCash()<b) {
				msg.setText("Not enough cash");
			}
			else{
				msg.setText(game.playRound(oddeven(),b));
				cash.setText("Money: "+game.getCash());
			}
			
		}
		catch(Exception error){
			msg.setText("NOT AN INTEGER"+bet.getText());
			cash.setText("Money "+game.getCash());

		}


	}
	/**
	 * Decides if its odd or even
	 * 
	 */
	public String oddeven() {
		int i=Integer.parseInt(choice.getText());
		if(i%2==0) {
			return "even";
		}
		return "odd";
	}
}
