package diceroll;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Start extends Application{
	private diceroll game;
	
	@Override
	public void start(Stage stage){
		Group root = new Group(); 
		game=new diceroll();
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
		//associate scene to stage and show
		stage.setTitle("diceroll"); 
		stage.setScene(scene); 
		
		HBox hbox=new HBox();
		TextField msg=new TextField("Messages");
		TextField bet=new TextField("Bet");
		TextField cash=new TextField("250");

		userchoice vbet=new userchoice(game,msg,cash,bet);
		bet.setOnAction(vbet);
		
		VBox vbox=new VBox(hbox,msg,cash,bet);
		for(int i=1;i<7;i++) {
			Button b=new Button(i+"");
			hbox.getChildren().add(b);
			userchoice choice=new userchoice(b);
			b.setOnAction(choice);
		}
		root.getChildren().addAll(vbox);
		
        stage.show();
        
	}
	public static void main(String[] args) {
		Application.launch(args);
	}

}
